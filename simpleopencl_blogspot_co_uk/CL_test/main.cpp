///
///	http://simpleopencl.blogspot.co.uk/2013/06/tutorial-simple-start-with-opencl-and-c.html
///	[25-04-2015]
///
#include "mainwindow.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

//	Note: cl.hpp is not installed with the Nvidia toolchain by default.
//	Get this file from: https://www.khronos.org/registry/cl/api/1.1/cl.hpp
#include <CL/cl.hpp>

using std::vector;
using std::cout;
using std::string;
using std::ifstream;

#define LOAD_EXTERNAL

string loadFromFile(string filename)
{
	ifstream file(filename);

	string result((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

	cout << "File: \n" << result << "\n";

	file.close();

	return result;
}

int main(int argc, char *argv[])
{
	////////////////////////////////////////////////////////////////////////////
	////	1. Find a platform												////
	////////////////////////////////////////////////////////////////////////////

	//	get all platforms (drivers)
	vector<cl::Platform> all_platforms;
	cl::Platform::get(&all_platforms);

	if (all_platforms.size() == 0)
	{
		//	Couldn't detect any platforms
		cout << "No platforms found.  Check OpenCL installation!\n";
		return 1;
	}
	else
	{
		//	List avaialable platforms
		for (unsigned long i = 0; i < all_platforms.size(); ++i)
		{
			cout << "Platform " << i << "\t";
			cout << all_platforms[i].getInfo<CL_PLATFORM_NAME>() << "\n";
		}
	}

	//	Use the first available platform
	cl::Platform default_platform=all_platforms[0];
	cout << "Using platform: ";
	cout << default_platform.getInfo<CL_PLATFORM_NAME>() << "\n";

	////////////////////////////////////////////////////////////////////////////
	////	2.	Find a device												////
	////////////////////////////////////////////////////////////////////////////

	//get default device of the default platform
	vector<cl::Device> all_devices;
	default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);

	if(all_devices.size()==0)
	{
		//	No device found
		cout<<" No devices found. Check OpenCL installation!\n";
		exit(1);
	}
	else
	{
		//	List avaialable devices
		for (unsigned long i = 0; i < all_devices.size(); ++i)
		{
			cout << "Device " << i << "\t";
			cout << all_devices[i].getInfo<CL_DEVICE_NAME>() << "\n";
		}
	}

	//	Use the first available device
	cl::Device default_device=all_devices[0];
	cout<< "Using device: ";
	cout << default_device.getInfo<CL_DEVICE_NAME>() << "\n";

	////////////////////////////////////////////////////////////////////////////
	////	3.	Load and build Kernels										////
	////////////////////////////////////////////////////////////////////////////

	//	Create our CL context to link the platform and this program
	cl::Context context({default_device});
	cl::Program::Sources sources;

#ifdef LOAD_EXTERNAL
    string kernel_code = loadFromFile("test.cl");
#else
	// kernel calculates for each element C=A+B
	string kernel_code=
			"void kernel simple_add(global const int* A, global const int* B, global int* C)"
			"{"
			"	C[get_global_id(0)]=A[get_global_id(0)]+B[get_global_id(0)];"
			"}";
#endif
	sources.push_back({kernel_code.c_str(),kernel_code.length()});

	cl::Program program(context,sources);
	if(program.build({default_device}) == CL_SUCCESS)
	{
		cout <<"Kernel built sucessfully.\n";
	}
	else
	{
		cout <<" Error building: ";
		cout << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device);
		cout <<"\n";
		return(1);
	}

	////////////////////////////////////////////////////////////////////////////
	////	4.	Create and fill buffers										////
	////////////////////////////////////////////////////////////////////////////

	// create buffers on the device
	cl::Buffer buffer_A(context,CL_MEM_READ_WRITE,sizeof(int)*10);
	cl::Buffer buffer_B(context,CL_MEM_READ_WRITE,sizeof(int)*10);
	cl::Buffer buffer_C(context,CL_MEM_READ_WRITE,sizeof(int)*10);

	//	data for buffer
	int A[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	int B[] = {0, 1, 2, 0, 1, 2, 0, 1, 2, 0};

	////////////////////////////////////////////////////////////////////////////
	////	5.	Create command queue, write data to device					////
	////////////////////////////////////////////////////////////////////////////

	//create queue to which we will push commands for the device.
	cl::CommandQueue queue(context,default_device);

	//write arrays A and B to the device
	queue.enqueueWriteBuffer(buffer_A,CL_TRUE,0,sizeof(int)*10,A);
	queue.enqueueWriteBuffer(buffer_B,CL_TRUE,0,sizeof(int)*10,B);

	////////////////////////////////////////////////////////////////////////////
	////	6.	Create the Kernel and execute								////
	////////////////////////////////////////////////////////////////////////////

	//	Create Kernel function
	cl::KernelFunctor simple_add(cl::Kernel(program,"simple_add"),
								 queue,
								 cl::NullRange,
								 cl::NDRange(10),
								 cl::NullRange);

	//	Execute
	simple_add(buffer_A, buffer_B, buffer_C);

	////////////////////////////////////////////////////////////////////////////
	////	7.	Retreive results											////
	////////////////////////////////////////////////////////////////////////////

	int C[10];

	//read result C from the device to array C
	queue.enqueueReadBuffer(buffer_C,CL_TRUE,0,sizeof(int)*10,C);

	cout<<"result: \n";
	for(int i=0;i<10;i++)
	{
		cout<<C[i]<<" ";
	}

	////////////////////////////////////////////////////////////////////////////
	////	8.	Finished													////
	////////////////////////////////////////////////////////////////////////////

	return 0;
}
