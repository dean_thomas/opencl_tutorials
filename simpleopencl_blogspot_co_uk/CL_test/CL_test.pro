#-------------------------------------------------
#
# Project created by QtCreator 2015-04-25T14:55:16
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++11

#   GCC seems to not like the cl.hpp header unless put into permissive mode
QMAKE_CXX += -fpermissive

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CL_test
TEMPLATE = app


SOURCES += main.cpp

HEADERS  +=

FORMS    +=

##  For MacBook
##
LIBS += -L"C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v5.5/lib/Win32/" -lOpenCL
INCLUDEPATH += "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v5.5/include"
DEPENDPATH += "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v5.5/include"

##  For UniPC
##
LIBS += -L$$PWD"/../../../../Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0/lib/x64/" -lOpenCL
INCLUDEPATH += $$PWD"/../../../../Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0/include"
DEPENDPATH += $$PWD"/../../../../Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0/include"

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD"/../../../../Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0/lib/x64/libOpenCL.a"
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD"/../../../../Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0/lib/x64/libOpenCLd.a"
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD"/../../../../Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0/lib/x64/OpenCL.lib"
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD"/../../../../Program Files/NVIDIA GPU Computing Toolkit/CUDA/v7.0/lib/x64/OpenCLd.lib"

DISTFILES += \
    test.cl
